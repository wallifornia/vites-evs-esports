# VITES #



### What is VITES? ###

**VITES (Video Integrated Technology for Esports Streaming)** is a cloud-based **VSPPaaS (Video Streaming Production Platform as a Service)** 
that enables pro esport players to stream and enhance their gameplay with real-time communications features 
without needing to build backend infrastructure and interfaces. 

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact